package utilities

func Contains(slice []string, val string) bool {
	for _, item := range slice {
		if val == item {
			return true
		}
	}

	return false
}
