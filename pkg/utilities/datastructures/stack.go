package datastructures

type Stack struct {
	values []interface{}
}

func NewStack() *Stack {
	return &Stack{[]interface{}{}}
}

func (stack *Stack) Push(item interface{}) {
	stack.values = append(stack.values, item)
}

func (stack *Stack) Pop() (item interface{}) {
	item = stack.Top()
	stack.values = stack.values[0 : len(stack.values)-1]
	return
}

func (stack Stack) Top() (item interface{}) {
	return stack.values[len(stack.values)-1]
}

func (stack Stack) Size() int {
	return len(stack.values)
}

func (stack *Stack) Clear() {
	stack.values = []interface{}{}
}
