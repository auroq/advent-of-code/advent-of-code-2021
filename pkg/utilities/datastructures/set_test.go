package datastructures_test

import (
	"gitlab.com/auroq/advent-of-code/advent-of-code-2021/pkg/utilities/datastructures"
	"testing"
)

func TestNewSet(t *testing.T) {
	t.Run("when instantiating empty set", func(t *testing.T) {
		set := datastructures.NewSet()
		if set.Count() != 0 {
			t.Fatal("New set should start empty")
		}
	})
	t.Run("when instantiating a set with items", func(t *testing.T) {
		items := []interface{}{1, 2, 3, 4, 5}
		set := datastructures.NewSet(items...)
		if set.Count() != 5 {
			t.Fatal("New set size should be number of items")
		}
		for _, item := range items {
			if !set.Contains(item) {
				t.Fatalf("Added item '%d' should exist in the set", item)
			}
		}
	})
}

func TestNewRuneSet(t *testing.T) {
	t.Run("when instantiating empty set", func(t *testing.T) {
		set := datastructures.NewRuneSet()
		if set.Count() != 0 {
			t.Fatal("New set should start empty")
		}
	})
	t.Run("when instantiating a set with items", func(t *testing.T) {
		items := []rune{'1', '2', '3', '4', '5'}
		set := datastructures.NewRuneSet(items...)
		if set.Count() != 5 {
			t.Fatal("New set size should be number of items")
		}
		for _, item := range items {
			if !set.Contains(item) {
				t.Fatalf("Added item '%d' should exist in the set", item)
			}
		}
	})
}

func TestNewRuneSetFromString(t *testing.T) {
	t.Run("when instantiating a set with items", func(t *testing.T) {
		items := []rune{'1', '2', '3', '4', '5'}
		set := datastructures.NewRuneSetFromString(string(items))
		if set.Count() != 5 {
			t.Fatal("New set size should be number of items")
		}
		for _, item := range items {
			if !set.Contains(item) {
				t.Fatalf("Added item '%c' should exist in the set", item)
			}
		}
	})
}

func TestNewStringSet(t *testing.T) {
	t.Run("when instantiating empty set", func(t *testing.T) {
		set := datastructures.NewSet()
		if set.Count() != 0 {
			t.Fatal("New set should start empty")
		}
	})
	t.Run("when instantiating a set with items", func(t *testing.T) {
		items := []string{"1", "2", "3", "4", "5"}
		set := datastructures.NewStringSet(items...)
		if set.Count() != 5 {
			t.Fatal("New set size should be number of items")
		}
		for _, item := range items {
			if !set.Contains(item) {
				t.Fatalf("Added item '%s' should exist in the set", item)
			}
		}
	})
}

func TestSet_Add(t *testing.T) {
	t.Run("when adding a single item", func(t *testing.T) {
		set := datastructures.NewSet()
		set.Add(10)
		if set.Count() != 1 {
			t.Fatal("Count of set should be 1")
		}
		if !set.Contains(10) {
			t.Fatal("Added item should exist in the set")
		}
	})

	t.Run("when adding same item twice", func(t *testing.T) {
		set := datastructures.NewSet()
		set.Add(10)
		set.Add(10)
		if set.Count() != 1 {
			t.Fatal("Count of set should be 1")
		}
		if !set.Contains(10) {
			t.Fatal("Added item should exist in the set")
		}
	})

	t.Run("when adding multiple items", func(t *testing.T) {
		set := datastructures.NewSet()
		set.Add(10)
		set.Add(-2)
		if set.Count() != 2 {
			t.Fatal("Count of set should be 1")
		}
		if !set.Contains(10) {
			t.Fatal("Added item 10 should exist in the set")
		}
		if !set.Contains(-2) {
			t.Fatal("Added item -2 should exist in the set")
		}
	})
}

func TestSet_Remove(t *testing.T) {
	t.Run("when removing an item that exists", func(t *testing.T) {
		set := datastructures.NewSet(1, 2, 3, 4, 5)
		set.Remove(3)
		if set.Count() != 4 {
			t.Fatal("Count of set should be 4")
		}
		if set.Contains(3) {
			t.Fatal("Removed item item 3 should not exist in the set")
		}
	})

	t.Run("when removing an item that does not exists", func(t *testing.T) {
		set := datastructures.NewSet(1, 2, 3, 4, 5)
		set.Remove(10)
		if set.Count() != 5 {
			t.Fatal("Count of set should be 5")
		}
		if set.Contains(10) {
			t.Fatal("Item 10 should still not exist in the set")
		}
	})
}
