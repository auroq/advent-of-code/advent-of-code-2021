package aoc

import (
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
)

const baseUrl = "https://adventofcode.com/2021"

func getSession() (session string, err error) {
	session = os.Getenv("AOC_SESSION")
	if len(session) <= 0 {
		err = errors.New("AOC_SESSION has not been set")
	}

	return
}

func GetInput(day int) (input []string, err error) {
	session, err := getSession()
	if err != nil {
		return
	}

	url := fmt.Sprintf("%s/day/%d/input", baseUrl, day)

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return
	}

	req.Header.Add("cookie", fmt.Sprintf("session=%s", session))

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return
	}
	defer res.Body.Close()
	if res.StatusCode < 200 || res.StatusCode > 299 {
		return nil, fmt.Errorf("received status code %d while attempting to pull input", res.StatusCode)
	}

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return
	}
	input = strings.Split(string(body), "\n")

	// Trim empty lines from end of slice
	for input[len(input)-1] == "" {
		input = input[:len(input)-1]
	}
	return
}

func Submit(day, part int, answer string) error {
	session, err := getSession()
	if err != nil {
		return err
	}
	url := fmt.Sprintf("%s/day/%d/answer", baseUrl, day)

	payload := strings.NewReader(fmt.Sprintf("level=%d&answer=%v", part, answer))

	req, _ := http.NewRequest("POST", url, payload)

	req.Header.Add("content-type", "application/x-www-form-urlencoded")
	req.Header.Add("cookie", fmt.Sprintf("session=%s", session))

	res, _ := http.DefaultClient.Do(req)

	if res != nil {
		defer res.Body.Close()
	}
	if res.StatusCode < 200 || res.StatusCode > 299 {
		return fmt.Errorf("received status code %d while attempting to submit", res.StatusCode)
	}
	body, _ := ioutil.ReadAll(res.Body)

	fmt.Println(res)
	fmt.Println(string(body))

	return nil
}
