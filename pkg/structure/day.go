package structure

import (
	"bufio"
	"fmt"
	"gitlab.com/auroq/advent-of-code/advent-of-code-2021/pkg/aoc"
	"io/ioutil"
	"os"
	"path"
	"path/filepath"
	"strconv"
	"strings"
)

func (day Day) getInput() (input []string, err error) {
	if _, err = os.Stat(day.inputPath); os.IsNotExist(err) {
		fmt.Printf("input.txt for challenge did not exist at '%s'.\nAttempting to download it from adventofcode.com...\n", day.inputPath)
		input, err = aoc.GetInput(day.number)
		if err != nil {
			return
		}
		fmt.Printf("Download successful!\nAttempting to write input to '%s' for future use...\n", day.inputPath)
		err = day.writeInput(input)
		if err != nil {
			fmt.Printf("Error while writing file:\n%v\n\n", err)
		}
		fmt.Println("Download was successful")
		return
	} else {
		return day.readInput()
	}
}

func (day Day) readInput() ([]string, error) {
	content, err := ioutil.ReadFile(day.inputPath)
	if err != nil {
		return nil, err
	}
	input := strings.Split(string(content), "\n")

	// Trim empty lines from end of slice
	for input[len(input)-1] == "" {
		input = input[:len(input)-1]
	}
	return input, nil
}

func (day Day) writeInput(contents []string) error {
	err := os.MkdirAll(filepath.Dir(day.inputPath), os.ModePerm)
	if err != nil {
		return err
	}

	file, err := os.Create(day.inputPath)
	if err != nil {
		return err
	}
	defer file.Close()

	w := bufio.NewWriter(file)
	for _, line := range contents {
		_, err = fmt.Fprintln(w, line)
		if err != nil {
			return err
		}
	}
	return w.Flush()
}

type Day struct {
	number    int
	inputPath string
	part1     Part
	part2     Part
}

func NewDay(number int, part1 Part, part2 Part) Day {
	inputPath := path.Join("inputs", fmt.Sprintf("day%02d-input.txt", number))
	return Day{
		inputPath: inputPath,
		number:    number,
		part1:     part1,
		part2:     part2,
	}
}

func (day Day) Run(num int) (answer1Str, answer2Str string, err error) {
	input, err := day.getInput()
	if err != nil {
		return
	}

	var answer int
	if num <= 1 || num > 2 {
		answer, err = day.part1.Run(input)
		if err != nil {
			return
		}
		answer1Str = strconv.Itoa(answer)
	}
	if num < 1 || num >= 2 {
		if day.part2 == nil {
			err = fmt.Errorf("no part %d for day %d", num, day.number)
			return
		}
		answer, err = day.part2.Run(input)
		if err != nil {
			return
		}
		answer2Str = strconv.Itoa(answer)
	}
	return
}
