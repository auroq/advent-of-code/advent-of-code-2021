package part2

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPart2_Run(t *testing.T) {
	var inputs = []string{}
	expected := -1
	part2 := Part2{}
	actual, err := part2.Run(inputs)
	if err != nil {
		if err.Error() == "challenge not yet implemented" {
			t.Skip("challenge not yet implemented")
		}
		t.Error(err)
	}
	assert.Equal(t, expected, actual)
}
