package part1

import (
	"errors"
)

type Part1 struct{}

func (part Part1) Run(input []string) (answer int, err error) {
	return 0, errors.New("challenge not yet implemented")
}
