package part1

import (
	"strconv"
	"strings"
)

type Part1 struct{}

func (part Part1) Run(input []string) (answer int, err error) {
	rotation := rotate(input)
	gma := gamma(rotation)
	eps := epsilon(gma)

	gmaDecimal, err := strconv.ParseInt(gma, 2, 32)
	if err != nil {
		return
	}
	epsDecimal, err := strconv.ParseInt(eps, 2, 32)
	if err != nil {
		return
	}

	answer = int(gmaDecimal * epsDecimal)
	return
}

func rotate(input []string) []string {
	output := make([]string, len(input[0]))

	for _, line := range input {
		for col := 0; col < len(line); col++ {
			output[col] += string(line[col])
		}
	}

	return output
}

func gamma(input []string) string {
	output := ""

	for _, line := range input {
		sumOnes := len(strings.ReplaceAll(line, "0", ""))
		sumZeros := len(strings.ReplaceAll(line, "1", ""))
		if sumOnes > sumZeros {
			output += "1"
		} else {
			output += "0"
		}
	}
	return output
}

func epsilon(gamma string) string {
	var convert func(acc, input string) string
	convert = func(acc, input string) string {
		if len(input) <= 0 {
			return acc
		}

		if string(input[0]) == "0" {
			return convert(acc+"1", input[1:])
		} else {
			return convert(acc+"0", input[1:])
		}
	}
	return convert("", gamma)
}
