package part1

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPart1_Run(t *testing.T) {
	var inputs = []string{
		"00100",
		"11110",
		"10110",
		"10111",
		"10101",
		"01111",
		"00111",
		"11100",
		"10000",
		"11001",
		"00010",
		"01010",
	}
	expected := 198
	part1 := Part1{}
	actual, err := part1.Run(inputs)
	if err != nil {
		if err.Error() == "challenge not yet implemented" {
			t.Skip("challenge not yet implemented")
		}
		t.Error(err)
	}
	assert.Equal(t, expected, actual)
}
func TestEpsilon(t *testing.T) {
	gamma := "10110"
	expected := "01001"
	actual := epsilon(gamma)
	assert.Equal(t, expected, actual)
}

func TestGama(t *testing.T) {
	var inputs = []string{
		"011110011100",
		"010001010101",
		"111111110000",
		"011101100011",
		"000111100100",
	}
	actual := gamma(inputs)
	expected := "10110"
	assert.Equal(t, expected, actual)
}

func TestRotate(t *testing.T) {
	var inputs = []string{
		"00100",
		"11110",
		"10110",
		"10111",
		"10101",
		"01111",
		"00111",
		"11100",
		"10000",
		"11001",
		"00010",
		"01010",
	}
	var expected = []string{
		"011110011100",
		"010001010101",
		"111111110000",
		"011101100011",
		"000111100100",
	}
	actual := rotate(inputs)
	assert.ElementsMatch(t, expected, actual)
}
