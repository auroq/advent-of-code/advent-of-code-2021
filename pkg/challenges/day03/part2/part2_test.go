package part2

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPart2_Run(t *testing.T) {
	var inputs = []string{
		"00100",
		"11110",
		"10110",
		"10111",
		"10101",
		"01111",
		"00111",
		"11100",
		"10000",
		"11001",
		"00010",
		"01010",
	}
	expected := 230
	part2 := Part2{}
	actual, err := part2.Run(inputs)
	if err != nil {
		if err.Error() == "challenge not yet implemented" {
			t.Skip("challenge not yet implemented")
		}
		t.Error(err)
	}
	assert.Equal(t, expected, actual)
}

func TestGetO2(t *testing.T) {
	var inputs = []string{
		"00100",
		"11110",
		"10110",
		"10111",
		"10101",
		"01111",
		"00111",
		"11100",
		"10000",
		"11001",
		"00010",
		"01010",
	}
	actual := getO2(inputs)
	expected := "10111"
	assert.Equal(t, expected, actual)
}

func TestGetCO2(t *testing.T) {
	var inputs = []string{
		"00100",
		"11110",
		"10110",
		"10111",
		"10101",
		"01111",
		"00111",
		"11100",
		"10000",
		"11001",
		"00010",
		"01010",
	}
	actual := getCO2(inputs)
	expected := "01010"
	assert.Equal(t, expected, actual)
}
