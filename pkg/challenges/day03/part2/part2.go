package part2

import (
	"strconv"
	"strings"
)

type Part2 struct{}

func (part Part2) Run(input []string) (answer int, err error) {
	o2 := getO2(input)
	co2 := getCO2(input)

	o2Dec, err := strconv.ParseInt(o2, 2, 32)
	if err != nil {
		return
	}
	co2Dec, err := strconv.ParseInt(co2, 2, 32)
	if err != nil {
		return
	}

	answer = int(o2Dec * co2Dec)
	return
}

func getCol(input []string, index int) string {
	output := ""

	for _, line := range input {
		output += string(line[index])
	}

	return output
}

func keeper(input string, most bool) string {
	sumOnes := len(strings.ReplaceAll(input, "0", ""))
	sumZeros := len(strings.ReplaceAll(input, "1", ""))
	if sumOnes >= sumZeros {
		if most {
			return "1"
		}
		return "0"
	} else {
		if most {
			return "0"
		}
		return "1"
	}
}

func getCO2(input []string) string {
	return iterate(0, input, false)
}

func getO2(input []string) string {
	return iterate(0, input, true)
}

func iterate(colIndex int, input []string, most bool) string {
	if len(input) == 1 {
		return input[0]
	}

	var newInput []string

	col := getCol(input, colIndex)
	keep := keeper(col, most)

	for _, line := range input {
		if string(line[colIndex]) == keep {
			newInput = append(newInput, line)
		}
	}

	return iterate(colIndex+1, newInput, most)
}
