package part2

import (
	"errors"
)

type Part2 struct{}

func (part Part2) Run(input []string) (answer int, err error) {
	return 0, errors.New("challenge not yet implemented")
}
