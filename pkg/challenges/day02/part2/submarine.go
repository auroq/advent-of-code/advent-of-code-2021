package part2

type Submarine struct {
	Depth      int
	Horizontal int
	Aim        int
}

func NewSubmarine() Submarine {
	return Submarine{
		Depth:      0,
		Horizontal: 0,
		Aim:        0,
	}
}

func (sub Submarine) Forward(value int) Submarine {
	return Submarine{
		Aim:        sub.Aim,
		Depth:      sub.Depth + value*sub.Aim,
		Horizontal: sub.Horizontal + value,
	}
}

func (sub Submarine) Down(value int) Submarine {
	return Submarine{
		Aim:        sub.Aim + value,
		Depth:      sub.Depth,
		Horizontal: sub.Horizontal,
	}
}

func (sub Submarine) Up(value int) Submarine {
	return Submarine{
		Aim:        sub.Aim - value,
		Depth:      sub.Depth,
		Horizontal: sub.Horizontal,
	}
}
