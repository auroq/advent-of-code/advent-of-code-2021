package part2

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPart2_Run(t *testing.T) {
	var inputs = []string{
		"forward 5",
		"down 5",
		"forward 8",
		"up 3",
		"down 8",
		"forward 2",
	}
	expected := 900
	part2 := Part2{}
	actual, err := part2.Run(inputs)
	if err != nil {
		if err.Error() == "challenge not yet implemented" {
			t.Skip("challenge not yet implemented")
		}
		t.Error(err)
	}
	assert.Equal(t, expected, actual)
}

func TestRunPrime(t *testing.T) {
	var inputs = []string{
		"forward 5",
		"down 5",
		"forward 8",
		"up 3",
		"down 8",
		"forward 2",
	}
	expectedDepth := 60
	expectedHorizontal := 15
	actual, err := runPrime(NewSubmarine(), inputs)

	assert.Nil(t, err)
	assert.Equal(t, expectedHorizontal, actual.Horizontal)
	assert.Equal(t, expectedDepth, actual.Depth)
}
