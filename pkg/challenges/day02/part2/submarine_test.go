package part2

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSumbarine_Forward(t *testing.T) {
	inputs := []struct {
		value         int
		depth         int
		aim           int
		expectedDepth int
	}{
		{
			value:         1,
			depth:         0,
			aim:           0,
			expectedDepth: 0,
		}, {
			value:         5,
			depth:         0,
			aim:           0,
			expectedDepth: 0,
		}, {
			value:         1,
			depth:         0,
			aim:           1,
			expectedDepth: 1,
		}, {
			value:         5,
			depth:         0,
			aim:           1,
			expectedDepth: 5,
		}, {
			value:         5,
			depth:         0,
			aim:           5,
			expectedDepth: 25,
		}, {
			value:         1,
			depth:         1,
			aim:           0,
			expectedDepth: 1,
		}, {
			value:         1,
			depth:         1,
			aim:           1,
			expectedDepth: 2,
		}, {
			value:         5,
			depth:         1,
			aim:           1,
			expectedDepth: 6,
		}, {
			value:         5,
			depth:         1,
			aim:           5,
			expectedDepth: 26,
		},
	}
	for _, test := range inputs {
		t.Run("", func(*testing.T) {
			sub := NewSubmarine()
			sub.Horizontal = 0
			sub.Depth = test.depth
			sub.Aim = test.aim

			actual := sub.Forward(test.value)

			assert.Equal(t, test.value, actual.Horizontal, "Horizontal should move by value")
			assert.Equal(t, test.expectedDepth, actual.Depth, "Depth should match")
		})
	}
}

func TestSubmarine_Down(t *testing.T) {
	inputs := []struct {
		value       int
		aim         int
		expectedAim int
	}{
		{
			value:       1,
			aim:         0,
			expectedAim: 1,
		}, {
			value:       1,
			aim:         1,
			expectedAim: 2,
		}, {
			value:       5,
			aim:         0,
			expectedAim: 5,
		}, {
			value:       1,
			aim:         5,
			expectedAim: 6,
		},
	}
	for _, test := range inputs {
		t.Run("", func(*testing.T) {
			sub := NewSubmarine()
			sub.Aim = test.aim

			actual := sub.Down(test.value)

			assert.Equal(t, test.expectedAim, actual.Aim, "Aim should match")
		})
	}
}

func TestSubmarine_Up(t *testing.T) {
	inputs := []struct {
		value       int
		aim         int
		expectedAim int
	}{
		{
			value:       1,
			aim:         1,
			expectedAim: 0,
		}, {
			value:       0,
			aim:         1,
			expectedAim: 1,
		}, {
			value:       5,
			aim:         5,
			expectedAim: 0,
		}, {
			value:       1,
			aim:         5,
			expectedAim: 4,
		},
	}
	for _, test := range inputs {
		t.Run("", func(*testing.T) {
			sub := NewSubmarine()
			sub.Aim = test.aim

			actual := sub.Up(test.value)

			assert.Equal(t, test.expectedAim, actual.Aim, "Aim should match")
		})
	}
}
