package part2

import (
	"strconv"
	"strings"
)

type Part2 struct{}

func (part Part2) Run(input []string) (answer int, err error) {
	sub, err := runPrime(NewSubmarine(), input)
	answer = sub.Depth * sub.Horizontal
	return
}

func runPrime(acc Submarine, commands []string) (Submarine, error) {
	if len(commands) <= 0 {
		return acc, nil
	}

	parts := strings.Split(commands[0], " ")
	value, err := strconv.Atoi(parts[1])
	if err != nil {
		return acc, err
	}

	var f func(value int) Submarine

	switch parts[0] {
	case "forward":
		f = acc.Forward
	case "up":
		f = acc.Up
	case "down":
		f = acc.Down
	}

	return runPrime(f(value), commands[1:])
}
