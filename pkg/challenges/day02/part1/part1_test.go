package part1

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPart1_Run(t *testing.T) {
	var inputs = []string{
		"forward 5",
		"down 5",
		"forward 8",
		"up 3",
		"down 8",
		"forward 2",
	}
	expected := 150
	part1 := Part1{}
	actual, err := part1.Run(inputs)
	if err != nil {
		if err.Error() == "challenge not yet implemented" {
			t.Skip("challenge not yet implemented")
		}
		t.Error(err)
	}
	assert.Equal(t, expected, actual)
}

func TestRunPrime(t *testing.T) {
	var inputs = []string{
		"forward 5",
		"down 5",
		"forward 8",
		"up 3",
		"down 8",
		"forward 2",
	}
	expectedDepth := 10
	expectedHorizontal := 15
	actualDepth, actualHorizontal, err := runPrime(0, 0, inputs)

	assert.Nil(t, err)
	assert.Equal(t, expectedHorizontal, actualHorizontal)
	assert.Equal(t, expectedDepth, actualDepth)
}

func TestIterate(t *testing.T) {
	inputs := []struct {
		command            string
		expectedDepth      int
		expectedHorizontal int
	}{
		{
			command:            "forward 1",
			expectedDepth:      0,
			expectedHorizontal: 1,
		}, {
			command:            "up 1",
			expectedDepth:      -1,
			expectedHorizontal: 0,
		}, {
			command:            "down 1",
			expectedDepth:      1,
			expectedHorizontal: 0,
		},
	}
	for _, test := range inputs {
		t.Run(test.command, func(*testing.T) {
			actualDepth, actualHorizontal, err := iterate(0, 0, test.command)
			assert.Nil(t, err)
			assert.Equal(t, test.expectedDepth, actualDepth)
			assert.Equal(t, test.expectedHorizontal, actualHorizontal)
		})
	}
}
