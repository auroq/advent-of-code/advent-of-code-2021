package part1

import (
	"strconv"
	"strings"
)

type Part1 struct{}

func (part Part1) Run(input []string) (answer int, err error) {
	depth, horizontal, err := runPrime(0, 0, input)
	answer = depth * horizontal
	return
}

func runPrime(depth, horizontal int, commands []string) (newDepth, newHorizontal int, err error) {
	if len(commands) <= 0 {
		return depth, horizontal, nil
	}

	newDepth, newHorizontal, err = iterate(depth, horizontal, commands[0])
	return runPrime(newDepth, newHorizontal, commands[1:])
}

func iterate(curDepth, curHorizontal int, command string) (newDepth int, newHorizontal int, err error) {
	newDepth = curDepth
	newHorizontal = curHorizontal

	parts := strings.Split(command, " ")
	value, err := strconv.Atoi(parts[1])
	if err != nil {
		return curDepth, curHorizontal, err
	}

	switch parts[0] {
	case "forward":
		newHorizontal += value
	case "up":
		newDepth -= value
	case "down":
		newDepth += value
	}

	return
}
