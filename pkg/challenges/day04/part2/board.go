package part2

import (
	"sort"
	"strconv"
	"strings"

	"gitlab.com/auroq/advent-of-code/advent-of-code-2021/pkg/utilities"
)

var winPermutations = [][]string{
	{"A1", "A2", "A3", "A4", "A5"},
	{"B1", "B2", "B3", "B4", "B5"},
	{"C1", "C2", "C3", "C4", "C5"},
	{"D1", "D2", "D3", "D4", "D5"},
	{"E1", "E2", "E3", "E4", "E5"},

	{"A1", "B1", "C1", "D1", "E1"},
	{"A2", "B2", "C2", "D2", "E2"},
	{"A3", "B3", "C3", "D3", "E3"},
	{"A4", "B4", "C4", "D4", "E4"},
	{"A5", "B5", "C5", "D5", "E5"},

	//{"A1", "B2", "C3", "D4", "E5"},
	//{"A5", "B4", "C3", "D2", "E1"},
}

type Board struct {
	values   map[int]string
	unmarked map[string]int
	hits     []string
}

func (board *Board) call(val int) bool {
	if position, ok := board.values[val]; ok {
		board.hits = append(board.hits, position)
		sort.Strings(board.hits)
		delete(board.unmarked, position)
	}

	return board.hasWon()
}

func (board *Board) sumUnmarked() int {
	sum := 0

	for _, val := range board.unmarked {
		sum += val
	}

	return sum
}

func (board *Board) hasWon() bool {
	for _, perm := range winPermutations {
		equal := true
		for _, val := range perm {
			if !utilities.Contains(board.hits, val) {
				equal = false
				break
			}
		}

		if equal {
			return true
		}
	}

	return false
}

func NewBoard(rows []string) (*Board, error) {
	board := Board{
		values:   make(map[int]string),
		unmarked: make(map[string]int),
		hits:     []string{},
	}
	var nums []int
	for _, row := range rows {
		next, err := utilities.AsToIs(strings.Split(strings.ReplaceAll(strings.TrimSpace(row), "  ", " "), " "))
		if err != nil {
			return nil, err
		}
		nums = append(nums, next...)
	}

	for i, row := range []string{"A", "B", "C", "D", "E"} {
		for col := 1; col <= 5; col++ {
			board.values[nums[5*i+col-1]] = row + strconv.Itoa(col)
			board.unmarked[row+strconv.Itoa(col)] = nums[5*i+col-1]
		}
	}

	return &board, nil
}
