package part2

import (
	"strings"

	"gitlab.com/auroq/advent-of-code/advent-of-code-2021/pkg/utilities"
)

type Part2 struct{}

func (part Part2) Run(input []string) (answer int, err error) {
	nums, err := utilities.AsToIs(strings.Split(input[0], ","))
	if err != nil {
		return -1, err
	}

	input = input[2:]

	var boards []*Board
	for i := 0; i < len(input); i += 6 {
		next, err := NewBoard(input[i : i+5])
		if err != nil {
			return -1, err
		}
		boards = append(boards, next)
	}

	var lastBoard *Board
	lastNum := 0
	for _, num := range nums {
		lastNum = num
		boards = iterate(boards, num)
		if len(boards) == 1 {
			lastBoard = boards[0]
		}
		if len(boards) <= 0 {
			break
		}
	}

	answer = lastBoard.sumUnmarked() * lastNum

	return
}

func iterate(boards []*Board, num int) []*Board {
	var newBoards []*Board
	for _, board := range boards {
		if won := board.call(num); !won {
			newBoards = append(newBoards, board)
		}
	}

	return newBoards
}
