package part1

import (
	"strings"

	"gitlab.com/auroq/advent-of-code/advent-of-code-2021/pkg/utilities"
)

type Part1 struct{}

func (part Part1) Run(input []string) (answer int, err error) {
	nums, err := utilities.AsToIs(strings.Split(input[0], ","))
	if err != nil {
		return -1, err
	}

	input = input[2:]

	var boards []*Board
	for i := 0; i < len(input); i += 6 {
		next, err := NewBoard(input[i : i+5])
		if err != nil {
			return -1, err
		}
		boards = append(boards, next)
	}

	var board *Board = nil
	lastNum := 0
	for _, num := range nums {
		lastNum = num
		board = iterate(boards, num)
		if board != nil {
			break
		}
	}

	answer = board.sumUnmarked() * lastNum

	return
}

func iterate(boards []*Board, num int) *Board {
	for _, board := range boards {
		if won := board.call(num); won {
			return board
		}
	}

	return nil
}
