# Advent of Code 2021

Solutions for [Advent of Code 2021](https://adventofcode.com/2021) written in go.

## Building

A standalone binary of this program can be built using the following command.

```shell script
go build -o aoc cmd/aoc/main.go
```

For more advanced features see the [official Go documentation](https://golang.org/cmd/go/#hdr-Compile_packages_and_dependencies)

## Installation

This package can also be installed using the following command:

```shell script
go install gitlab.com/auroq/advent-of-code/advent-of-code-2021/cmd/aoc
```

## Running

This program can be run either from a binary (see above) or using `go run`.
See [official Go documentation](https://golang.org/cmd/go/#hdr-Compile_and_run_Go_program) for more information about `go run`.

### Specifying a Day (required)

A specific day can be run by executing running the program and passing the `-day` flag.

For example, the following will execute day 1.

```shell script
aoc  -day=1
```

### Specifying a Part of the Day

You may also specify either part 1 or part 2 by passing the `-part` flag

For example, the following will execute only part 2 of day 1.

```shell script
aoc  -day=1 -part=2
```

## Input

Each advent of code problem requires an input file.
The input files are expected to be located next to the program binary in a folder called "inputs".
These input files should be named with the format `day{NUM}-input.txt` where `{NUM}` is the two digit number of the day for the input.

If a given input file does not exist, it will be downloaded and created automatically.
In order for this to work, you must have your advent of code session ID specified in an environment variable called `AOC_SESSION`
This session ID can be found in the cookie for Advent of Code after you are logged in.


## Tests

Run all tests using `go test ./...`
