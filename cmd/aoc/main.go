package main

import (
	"flag"
	"fmt"
	"gitlab.com/auroq/advent-of-code/advent-of-code-2021/pkg/aoc"
	"gitlab.com/auroq/advent-of-code/advent-of-code-2021/pkg/challenges"
	"io/ioutil"
	"log"
	"os"
	"strings"
)

func main() {
	day, part, submit := parseFlags()
	if err := parseDotEnv(); err != nil {
		log.Fatal(err)
	}

	answer1, answer2, err := run(day, part)
	if err != nil {
		log.Fatal(err)
	}
	output(answer1, answer2)
	if submit {
		err := submitAnswer(day, answer1, answer2)
		if err != nil {
			log.Fatal(err)
		}
	}
}

func parseFlags() (day, part int, submit bool) {
	flag.IntVar(&day, "day", -1, "Day to execute")
	flag.IntVar(&part, "part", 0, "Part of day to execute")
	flag.BoolVar(&submit, "submit", false, "Whether to submit after execution")
	flag.Parse()
	return
}

func parseDotEnv() error {
	dotenv := ".env"
	if _, err := os.Stat(dotenv); os.IsNotExist(err) {
		return nil
	} else if err != nil {
		return err
	}

	content, err := ioutil.ReadFile(".env")
	if err != nil {
		return err
	}
	lines := strings.Split(string(content), "\n")
	for _, line := range lines {
		if line != "" {
			keyValue := strings.Split(line, "=")
			if err := os.Setenv(keyValue[0], keyValue[1]); err != nil {
				return err
			}
		}
	}

	return nil
}

func run(day, part int) (answer1, answer2 string, err error) {
	days := challenges.GetDays()

	if day < 1 || day > len(days) {
		err = fmt.Errorf("invalid day '%d'", day)
		return
	}
	if part < 0 || part > 2 {
		err = fmt.Errorf("invalid part '%d'", part)
		return
	}

	daySource := days[day-1]
	answer1, answer2, err = daySource.Run(part)

	return
}

func submitAnswer(day int, answer1, answer2 string) error {
	if len(answer1) > 0 {
		err := aoc.Submit(day, 1, answer1)
		if err != nil {
			return err
		}
	}
	if len(answer2) > 0 {
		err := aoc.Submit(day, 2, answer1)
		if err != nil {
			return err
		}
	}

	return nil
}

func output(answer1, answer2 string) {
	if len(answer1) > 0 {
		fmt.Printf("Part 1: %s\n", answer1)
	}
	if len(answer2) > 0 {
		fmt.Printf("Part 2: %s\n", answer2)
	}
}
